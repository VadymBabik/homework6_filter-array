"use strict";

let arr = [25, "Name", "number", Symbol(), 1n, 56, {}, null, true];
let typeValue = "string"; //number, bigint, boolean, string, symbol, object, function

function filterBy(array, value) {
  return array.filter(function (item) {
    return typeof item !== value;
  });
}

filterBy(arr, typeValue);
